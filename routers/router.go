package routers

import (
	"github.com/gin-gonic/gin"
	"github.com/epsilonv/blog/controllers"
)

func main() {
	router := gin.Default()
	router.GET("/", controllers.Index)
}