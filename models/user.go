package models

import (
	"github.com/jinzhu/gorm"
)

type User struct {
	gorm.Model
	Name string  `sql:"size:255;not null"`
	Nickmame string `sql:"size:255;not null"`
	Avatar string `sql:"size:255s"`
	Email string  `sql:"size:255;not null"`
	PasswordHash string `sql:"size:255;not null"`
	Description string
	Location string
	Posts []Post
	Replys []Reply
}

func (user *User) Insert() error {
	return DB.Create(user).Error
}

func (user *User) Update() error {
	return DB.Save(user).Error
}

func (user *User) GetPosts() error {

}

func