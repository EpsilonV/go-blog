package models

import "github.com/jinzhu/gorm"

type Post struct {
	gorm.Model
	UserID int32 `sql:"not null"`
	User User
	Title string `sql:"not null"`
	SubTitle string
	content string `sql:"type:text;not null"`
	status int32
	Replies []Reply
	RepliesCount int32 `sql:"not null;default:0"`
	ViewCount int32 `sql:"not null;default:0"`
}
