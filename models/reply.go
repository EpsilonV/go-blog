package models

import "github.com/jinzhu/gorm"

type Reply struct {
	gorm.Model
	UserID int32 `sql:"not null"`
	User User
	PostID int32 `sql:"not null"`
	Post Post
	Content string `sql:"type text; not null"`
}
